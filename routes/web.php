<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::model('order', 'App\\Models\\Orders');

Route::get('/', 'OrderController@getIndex');
Route::get('mail', 'OrderController@getMail');

Route::get('edit/{order}', 'OrderController@getEdit');
Route::post('edit/{order}', 'OrderController@postEdit');

Route::get('delete/{order}', 'OrderController@getDelete');