<?php

namespace App\Http\Controllers;

use Mail;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

use App\Http\Requests\OrderUpdateRequest;

use App\Models\Clients;
use App\Models\Products;
use App\Models\Orders;

use App\Mail\OrderReport;

class OrderController extends BaseController
{
	use DispatchesJobs, ValidatesRequests;

	/**
	 * Index page
	 * 
	 * @param  Request $request
	 * @return Response
	 */
	public function getIndex(Request $request)
	{
		$pagination = config('app.pagination.default');
		if ($request->input('full'))
			$pagination = null;

		$orders = with(new Orders)->listWithParams($request->all(), $pagination);
		$chart = with(new Orders)->prepareChartData($orders);

		return view('orders.index', ['orders' => $orders, 'chart' => $chart]);
	}

	/**
	 * Send mail
	 * 
	 * @param  Request $request
	 * @return Redirect
	 */
	public function getMail(Request $request)
	{
		$orders = with(new Orders)->listWithParams($request->all());

		Mail::to(config('app.report'))->send(new OrderReport($orders));

		return redirect(action('OrderController@getIndex'))
			->withMessagesSuccess(['Mail was sent successfully']);
	}

	/**
	 * Edit order form
	 * 
	 * @param  Request $request [description]
	 * @param  Orders  $order   [description]
	 * @return Response
	 */
	public function getEdit(Request $request, Orders $order)
	{
		$clients = Clients::get()->pluck('client', 'id');
		$products = Products::get()->pluck('product', 'id');

		return view('orders.edit', ['order' => $order, 'clients' => $clients, 'products' => $products]);
	}

	/**
	 * Update order
	 * 
	 * @param  OrderUpdateRequest $request
	 * @param  Orders             $order   [description]
	 * @return Reponse|Redirect
	 */
	public function postEdit(OrderUpdateRequest $request, Orders $order)
	{
		$order->fill($request->all());
		$order->push();

		return redirect(action('OrderController@getIndex'))
			->withMessagesSuccess(['Order was updated successfully']);
	}

	/**
	 * Delete order
	 * 
	 * @param  Request $request
	 * @param  Orders  $order
	 * @return Redirect
	 */
	public function getDelete(Request $request, Orders $order)
	{
		if ($order->delete())
			return redirect(action('OrderController@getIndex'))
				->withMessagesSuccess(['Order was deleted']);
		else
			return redirect(action('OrderController@getIndex'))
				->withErrors(['This order cannot be deleted']);
	}
}
