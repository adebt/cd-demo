<?php 

namespace App\Models;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'orders';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = ['id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * Allowed fields for filter
	 * 
	 * @var array
	 */
	protected $allowed_filter = ['all', 'client', 'product', 'total', 'date'];

	/**
	 * Allowed fields to sort orders
	 * 
	 * @var array
	 */
	protected $allowed_sort = ['client', 'product', 'date'];

	/**
	 * Allowed sort directions
	 * 
	 * @var array
	 */
	protected $allowed_direction = ['asc', 'desc'];

	/**
	 * One-to-One relation
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function client()
	{
		return $this->belongsTo('App\Models\Clients');
	}

	/**
	 * One-to-One relation
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function product()
	{
		return $this->belongsTo('App\Models\Products');
	}

	/**
	 * Get orders with filtration and sorting
	 * 
	 * @param  array $params
	 * @param  int|null $pagination
	 * @return Collection
	 */
	public function listWithParams($params, $pagination=null)
	{
		$query = $this->query()
			->select('orders.*');

		if ( ! empty($params['keyword']))
			$params['keyword'] = trim($params['keyword']);

		$field = 'all';
		if ( ! empty($params['field']) && in_array($params['field'], $this->allowed_filter))
			$field = $params['field'];

		if ( isset($params['keyword']))
		{
			$keyword = $params['keyword'];

			switch ($field) {
				case 'client':
					$this->filterClient($query, $keyword);
					break;
				
				case 'product':
					$this->filterProduct($query, $keyword);
					break;

				case 'total':
					$this->filterTotal($query, $keyword);
					break;

				case 'date':
					$this->filterDate($query, $keyword);
					break;

				default:
					$this->filterClient($query, $keyword, 'or');
					$this->filterProduct($query, $keyword, 'or');
					$this->filterTotal($query, $keyword, 'or');
					$this->filterDate($query, $keyword, 'or', true);
					break;
			}
		}

		if ( ! empty($params['sort']) && in_array($params['sort'], $this->allowed_sort))
		{
			$order = 'asc';
			if ( ! empty($params['order']) && in_array($params['order'], $this->allowed_direction))
				$order = $params['order'];

			switch ($params['sort']) {
				case 'client':
					$query->join('clients', 'clients.id', '=', 'orders.client_id')
						->orderBy('clients.client', $order);
					break;

				case 'product':
					$query->join('products', 'products.id', '=', 'orders.product_id')
						->orderBy('products.product', $order);
					break;
				
				default:
					$query->orderBy('created_at', $order);
					break;
			}
		}

		if ( ! is_null($pagination))
			$orders = $query->paginate($pagination)->appends($params);
		else
			$orders = $query->get();

		$orders->load(['client', 'product']);

		return $orders;
	}

	public function prepareChartData($orders)
	{
		$chart = [];

		$orders->each(function($item, $key) use(&$chart){
			$key = $item['created_at']->format('Y-m-d');
			if ( ! isset($chart[$key]))
				$chart[$key] = $item['total'];
			else
				$chart[$key] += $item['total'];
		});

		return $chart;
	}

	/**
	 * Filter by client name
	 * 
	 * @param  Builder $query
	 * @param  string $keyword
	 * @param  string $boolean
	 * @return void
	 */
	private function filterClient(&$query, $keyword, $boolean = 'and')
	{
		$method = 'whereHas';
		if ($boolean == 'or')
			$method = 'orWhereHas';

		$query->$method('client', function($query) use ($keyword){
			$query->where('client', 'like', '%' . $keyword . '%');
		});
	}

	/**
	 * Filter by product name
	 * 
	 * @param  Builder $query
	 * @param  string $keyword
	 * @param  string $boolean
	 * @return void
	 */
	private function filterProduct(&$query, $keyword, $boolean = 'and')
	{
		$method = 'whereHas';
		if ($boolean == 'or')
			$method = 'orWhereHas';

		$query->$method('product', function($query) use ($keyword){
			$query->where('product', 'like', '%' . $keyword . '%');
		});
	}

	/**
	 * Filter by order total
	 * 
	 * @param  Builder $query
	 * @param  string $keyword
	 * @param  string $boolean
	 * @return void
	 */
	private function filterTotal(&$query, $keyword, $boolean = 'and')
	{
		$query->where('total', '=', $keyword, $boolean);
	}

	/**
	 * Filter by order date
	 * 
	 * @param  Builder $query
	 * @param  string $keyword
	 * @param  string $boolean
	 * @return void
	 */
	private function filterDate(&$query, $keyword, $boolean = 'and', $safe = false)
	{
		$method = 'whereBetween';
		$raw = 'whereRaw';
		if ($boolean == 'or')
		{
			$method = 'orWhereBetween';
			$raw = 'orWhereRaw';
		}

		$date = strtotime($keyword);

		if ($date)
		{
			$query->$method('created_at', [date('Y-m-d 00:00:00', $date), date('Y-m-d 23:59:59', $date)], $boolean);
		}
		else
		{
			if ( ! $safe)
				$query->$raw('1 = 0');
		}
	}
}
