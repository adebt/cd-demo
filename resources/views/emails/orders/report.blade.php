<h1>Orders</h1>

<table border="1" width="100%">
	<thead>
		<tr>
			<th width="25%">Client</th>
			<th width="25%">Product</th>
			<th width="25%">Total</th>
			<th width="25%">Date</th>
		</tr>
	</thead>
	<tbody>
	@forelse($orders as $order)
		<tr>
			<td>{{ $order->client->client }}</td>
			<td>{{ $order->product->product }}</td>
			<td>${{ number_format($order->total, 2, '.', ',') }}</td>
			<td>{{ $order->created_at->format('m/d/Y') }}</td>
		</tr>
	@empty
		<tr>
			<td class="text-center" colspan="5">Nothing to show. Please, change Your keyword</td>
		</tr>
	@endforelse
	</tbody>
</table>