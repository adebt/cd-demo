<div class="row">
@if (count($errors) > 0)
	@foreach ($errors->all() as $error)
	<div class="alert alert-danger" role="alert">
		<strong>Error!</strong> {{ $error }}
	</div>	
	@endforeach
@endif

@if (session('messages_success'))
	<div class="alert alert-success" role="alert">
	@foreach (session('messages_success') as $message)
		<strong>Success!</strong> {{ $message }}
	@endforeach
	</div>	
@endif
</div>