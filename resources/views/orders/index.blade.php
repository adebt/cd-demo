@extends('layouts.app')

@section('page-header') Orders @endsection

@section('content')

@include('orders.parts.filter')
@include('orders.parts.chart')
@include('orders.parts.table')

@endsection