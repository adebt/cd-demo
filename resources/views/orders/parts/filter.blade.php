<div class="row">
	<div class="col-lg-12">
	<form class="form-inline" method="get" action="{{ action('OrderController@getIndex') }}">
		<div class="form-group">
			<input type="text" class="form-control" name="keyword" id="keyword" placeholder="Keyword" value="{{ Request::input('keyword') }}" />
		</div>
		<div class="form-group">
			<select class="form-control" name="field">
				<option value="all" @if(Request::input('field') == 'all')selected="selected"@endif>All</option>
				<option value="client" @if(Request::input('field') == 'client')selected="selected"@endif>Client</option>
				<option value="product" @if(Request::input('field') == 'product')selected="selected"@endif>Product</option>
				<option value="total" @if(Request::input('field') == 'total')selected="selected"@endif>Total</option>
				<option value="date" @if(Request::input('field') == 'date')selected="selected"@endif>Date</option>
			</select>
		</div>
		<button type="submit" class="btn btn-info">Search</button>
	</form>
	</div>
</div>