@if (count($chart))
<div class="row" style="margin: 35px 0;">
	<div class="col-lg-12">
		<div id="total-chart" style="height: 300px;"></div>
	</div>
</div>

@section('chart')
<script>
new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'total-chart',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data: [
  @foreach($chart as $date => $total)
    { date: '{{ $date }}', total: '{{ $total }}' },
  @endforeach
  ],
  // The name of the data record attribute that contains x-values.
  xkey: 'date',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['total'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Total']
});
</script>
@endsection
@endif