<div class="row">
	<div class="col-lg-12">
		<br />
		@if (count($orders))
		<a href="{{ action('OrderController@getMail', Request::all()) }}">Email this report</a> | <a href="{{ action('OrderController@getIndex', array_add(Request::all(), 'full', 1)) }}">Full table</a><br /><br />
		@endif
		
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th class="col-lg-3">
						<a href="{{ action('OrderController@getIndex', ['keyword' => Request::input('keyword'), 'field' => Request::input('field'), 'sort' => 'client', 'order' => (Request::input('order') == 'asc') ? 'desc' : 'asc']) }}">Client</a>
					</th>
					<th class="col-lg-3">
						<a href="{{ action('OrderController@getIndex', ['keyword' => Request::input('keyword'), 'field' => Request::input('field'), 'sort' => 'product', 'order' => (Request::input('order') == 'asc') ? 'desc' : 'asc']) }}">Product</a>
					</th>
					<th class="col-lg-2">
						Total
					</th>
					<th class="col-lg-2">
						<a href="{{ action('OrderController@getIndex', ['keyword' => Request::input('keyword'), 'field' => Request::input('field'), 'sort' => 'date', 'order' => (Request::input('order') == 'asc') ? 'desc' : 'asc']) }}">Date</a>
					</th>
					<th class="col-lg-2">
						Actions
					</th>
				</tr>
			</thead>
			<tbody>
			@forelse($orders as $order)
				<tr>
					<td>{{ $order->client->client }}</td>
					<td>{{ $order->product->product }}</td>
					<td>${{ number_format($order->total, 2, '.', ',') }}</td>
					<td>{{ $order->created_at->format('m/d/Y') }}</td>
					<td>
						<a href="{{ action('OrderController@getEdit', ['order' => $order]) }}">Edit</a> | 
						<a href="{{ action('OrderController@getDelete', ['order' => $order]) }}" onclick="if (!confirm('Are you sure?')) { return false; }">Delete</a>
					</td>
				</tr>
			@empty
				<tr>
					<td class="text-center" colspan="5">Nothing to show. Please, change Your keyword</td>
				</tr>
			@endforelse
			</tbody>
		</table>

		@if($orders instanceof \Illuminate\Pagination\LengthAwarePaginator )
			{{$orders->links()}}
		@endif
	</div>
</div>