@extends('layouts.app')

@section('page-header') Edit order @endsection

@section('content')
{!! Form::model($order) !!}
	<div class="form-group">
		{!! Form::label('client_id', 'Client') !!}
		{!! Form::select('client_id', $clients, null, ['class' => 'form-control']); !!}
	</div>

	<div class="form-group">
		{!! Form::label('product_id', 'Product') !!}
		{!! Form::select('product_id', $products, null, ['class' => 'form-control']); !!}
	</div>    

	<div class="form-group">
		{!! Form::label('total', 'Total') !!}
		{!! Form::text('total', null, ['class' => 'form-control']);  !!}
	</div>

	<div class="form-group">
		{!! Form::submit('Update order', ['class' => 'btn btn-large btn-success']) !!}
	</div>
{!! Form::close() !!}

@endsection